# This script should index the genome file specified in the first argument ($1),
# creating the index in a directory specified by the second argument ($2).
set -e
# The STAR command is provided for you. You should replace the parts surrounded by "<>" and uncomment it.
if [ -d $2 ] 
then
	echo "Index already generated for `basename $1`"
else
	echo "Generating index for $1"
	STAR --runThreadN 4 --runMode genomeGenerate --genomeDir $2 --genomeFastaFiles $1 --genomeSAindexNbases 9
fi
