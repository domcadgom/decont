# This script should download the file specified in the first argument ($1), place it in the directory specified in the second argument ($2), 
# and *optionally* uncompress the downloaded file with gunzip if the third argument ($3) contains the word "yes".
set -e

echo "Starting download..."
wget -nc -i $1 -P $2

if [ "$3" = "yes" ]
then
	gunzip -f -k $2/*.gz
fi	

