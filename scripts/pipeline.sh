#Set error checkout
set -e

#Download all the files specified in data/filenames
bash scripts/download.sh data/urls data


# Download the contaminants fasta file, and uncompress it
bash scripts/download.sh res/url res yes

# Index the contaminants file
bash scripts/index.sh res/contaminants.fasta res/contaminants_idx


# Create folder if not exists
mkdir -p out/merged

# Merge the samples into a single file
for sid in $(ls data/ | grep -e 'fastq\.gz$' | cut -d"-" -f1 | uniq)
do
    bash scripts/merge_fastqs.sh data out/merged $sid
done


# Create folder if not exists
mkdir -p out/trimmed
mkdir -p log/cutadapt


# Run cutadapt for all merged files
for sid in $(ls out/merged/ | grep -e  'fastq\.gz$' | cut -d"." -f1)	
do
	if [ -f "out/trimmed/${sid}.trimmed.fastq.gz" ]
	then
		echo "Cutadapt already done for ${sid}"
	else
		echo "Executing cutadapt for ${sid}"
		cutadapt -m 18 -a TGGAATTCTCGGGTGCCAAGG --discard-untrimmed -o out/trimmed/${sid}.trimmed.fastq.gz out/merged/${sid}.fastq.gz > log/cutadapt/${sid}_cutadapt.log
	fi
done

# STAR run
for fname in out/trimmed/*.fastq.gz
do
	sid=`basename ${fname} .trimmed.fastq.gz`
	if [ -f "out/star/${sid}/Log.final.out" ]
	then
		echo "STAR already executed for ${sid}"
	else
		echo "Executing STAR for ${sid}"
	    	mkdir -p out/star/${sid}

	    	STAR --runThreadN 4 --genomeDir res/contaminants_idx --outReadsUnmapped Fastx --readFilesIn ${fname} --readFilesCommand gunzip -c --outFileNamePrefix out/star/${sid}/
	fi
done 

# TODO: create a log file containing information from cutadapt and star logs
# (this should be a single log file, and information should be *appended* to it on each run)
# - cutadapt: Reads with adapters and total basepairs
# - star: Percentages of uniquely mapped reads, reads mapped to multiple loci, and to too many loci
# tip: use grep to filter the lines you're interested in


echo 'Autogenerated logs for the following files:' > log/global_log.log
for archivo in out/star/*
do
	final=`basename ${archivo}`
	echo ${final} 'logs:' >> log/global_log.log
	cat log/cutadapt/${final}_cutadapt.log | grep -e "Reads with adapters" -e "Total basepairs processed" >> log/global_log.log

	cat out/star/${final}/Log.final.out | grep -e "Uniquely mapped reads %" -e "% of reads mapped to multiple loci" -e "% of reads mapped to too many loci" >> log/global_log.log
done






